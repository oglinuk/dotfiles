# My Environment Configuration

The result of having to manually setup my environment one too many times.

## Usage

`. ./setup`

## TODO
* [X] Add check if package is installed (checkinstalled)
* [ ] Add verification of installations
* [ ] Add more than debian `apt` support for checkinstalled

## Packages
* alacritty
* autoconf
* cmake
* curl
* docker
* docker-compose
* git
* golang
* jq
* keepassxc
* lynx
* libsdl2
* make
* mpv
* mutt
* openvpn
* pandoc
* rust
* streamlink
* tmatrix
* tmux
* vim
* virtualbox
* vlc
* vscodium
* weechat
* youtube-dl
* yq
