colorscheme elflord
noremap <up> :echoerr "Pfft, only noobs use ^, use k instead"<CR>
noremap <down> :echoerr "Pfft, only noobs use v, use j instead"<CR>
noremap <left> :echoerr "Pfft, only noobs use <, use h instead"<CR>
noremap <right> :echoerr "Pfft, only noobs use >, use l instead"<CR>
set background=dark
set history=100
set number
set tabstop=2
set textwidth=73
set viminfo='20,<1000,s1000
syntax enable
